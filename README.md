# flow3r-ifconfig
## Displays simple network information

### Installation
Put your flow3r into disk mode https://docs.flow3r.garden/badge/programming.html#disk-mode
Put the files to sys/apps/ifconfig/
Press left shoulder-button to exit

### Usage
Go to Badge and use IP

### Exit
Press both shoulder-buttons

### Changelog
#### Version 3
Some fixes

#### Version 2
Navigate with shoulder buttons

#### Version 1
Makes the badge show ifconfig information
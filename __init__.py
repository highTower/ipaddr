from st3m.application import Application, ApplicationContext
from st3m.input import InputController, InputState
from st3m import logging
from st3m.goose import Optional
from ctx import Context
import leds

import math
import random
import st3m.run
import network
import time

log = logging.Log(__name__, level=logging.INFO)
log.info("time")



class Ip(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._wlan = network.WLAN()
        self._ifconfig = self._wlan.ifconfig()
        self._time = 0
        self._wait_time = 5000
        self._state = 0
        self._text = ("IP", "Subnet", "Gateway", "DNS", "MAC", "SSID", "Channel")
        self._lookup = ("mac", "ssid", "channel")
        self._value = "Loading"
        self._input = InputController()

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 30
        ctx.font = ctx.get_font_name(4)

        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.rgb(1,1,1)
        ctx.move_to(0, 0)
        ctx.save()
        ctx.scale(1.0, 1)
        ctx.text(self._value)
        ctx.restore()
        
        ctx.move_to(0, -60)
        ctx.save()
        ctx.font_size = 25
        ctx.scale(1.0, 1)
        ctx.text(self._text[self._state])
        ctx.restore()

    def think(self, ins: InputState, delta_ms: int) -> None:
        self._input.think(ins, delta_ms)
        
        if self._input.buttons.app.left.pressed:
            self._state -= 1
            self._time = 0
        elif self._input.buttons.app.right.pressed:
            self._state += 1
            self._time = 0
        
        self._time += delta_ms
        if self._time > self._wait_time :
            self._time = 0
            self._ifconfig = self._wlan.ifconfig()
            self._state += 1
        if self._state < 0 :
            self._state = 6
        if self._state > 6 :
            self._state = 0
            
        if self._state < 4:
            self._value = self._ifconfig[self._state]
        elif self._state == 4:
            self._value = self._wlan.config("mac").hex(":")
        else:
            self._value = str( self._wlan.config( self._lookup[self._state - 4] ) )
            
        
        super().think(ins, delta_ms)